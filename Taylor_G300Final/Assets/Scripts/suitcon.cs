﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class suitcon : MonoBehaviour {

	public Slider suitHealthBar;

	public static float suitHP = 100;
	public static float suitMaxHP = 100;

	public Transform damTextObj;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		if (suitHP <= 0) {

			battleflow.suitStatus = "dead";
			Destroy (gameObject);
		}

		if (((Input.GetKeyDown ("1"))||(battleflow.attButPressed == "y")) && (battleflow.whichTurn == 2)) 
		{
			battleflow.currentDamage = 40;
			battleflow.attButPressed = "n";
			GetComponent<Animator> ().SetTrigger ("suitMelee");
			GetComponent<Transform> ().position = new Vector2 (7.22f, -0.29f);

		}

		suitHealthBar.value = suitHP;
		
	}

	void returnSuit () {

		Instantiate (damTextObj, new Vector2 (9.2f, -.1f), damTextObj.rotation);
		battleflow.damageDisplay = "y";
		GetComponent<Transform> ().position = new Vector2 (-10.88f, -1.97f);
		battleflow.whichTurn = 3;
	}
}
