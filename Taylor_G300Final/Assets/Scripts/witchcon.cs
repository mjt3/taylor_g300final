﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class witchcon : MonoBehaviour {

	public Slider witchHealthBar;

	public Transform fireballObj;


	public static float witchHP = 50;
	public static float witchMaxHP = 50;

	public Transform damTextObj;

	// Use this for initialization
	void Start () {
		
	}

	// Update is called once per frame
	void Update () {

		if (witchHP <= 0) {
			
			battleflow.witchStatus = "dead";
			Destroy (gameObject);
		}

		if (((Input.GetKeyDown ("1"))||(battleflow.attButPressed == "y")) && (battleflow.whichTurn== 1)) 
		{
			battleflow.currentDamage = 80;
			battleflow.attButPressed = "n";
			GetComponent<Transform> ().position = new Vector2 (-4.36f, -0.22f);
			Instantiate (fireballObj, new Vector2 (-3.02f, .46f), fireballObj.rotation);
			StartCoroutine (returnWitch ());
		}

		witchHealthBar.value = witchHP;
			
		
	}

	IEnumerator returnWitch()
	{
		yield return new WaitForSeconds (1.65f);
		GetComponent<Transform> ().position = new Vector2 (-11.01f, 2.16f);
		battleflow.whichTurn = 2;
		Instantiate (damTextObj, new Vector2 (9.2f, -.1f), damTextObj.rotation);
		battleflow.damageDisplay = "y";
	}


		
}
