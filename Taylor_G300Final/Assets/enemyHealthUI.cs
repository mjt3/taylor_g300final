﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class enemyHealthUI : MonoBehaviour {

	public Slider enemy1HealthBar;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		GameObject pumpkin = GameObject.Find ("pumpkin");
		enemycon enemycon = pumpkin.GetComponent<enemycon>();
		enemy1HealthBar.value = enemycon.enemyHP;

	}
}
