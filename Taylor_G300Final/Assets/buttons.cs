﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class buttons : MonoBehaviour {

	public Sprite sword_img;

	public Sprite fireball_img;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		if (battleflow.whichTurn == 1) {

			GetComponent<SpriteRenderer> ().sprite = fireball_img;
		}

		if (battleflow.whichTurn == 2) {

			GetComponent<SpriteRenderer> ().sprite = sword_img;
		}

		
	}

	void OnMouseDown()
	{

		battleflow.attButPressed = "y";

	}
}
