﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class enemyHealthUI2 : MonoBehaviour {

	public Slider enemy2HealthBar;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
		GameObject pumpkin = GameObject.Find ("pumpkin2");
		enemycon enemycon = pumpkin.GetComponent<enemycon>();
		enemy2HealthBar.value = enemycon.enemyHP;

		
	}
}
