﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class battleflow : MonoBehaviour {

	public static int whichTurn = 1;
	public static float currentDamage = 0;

	public static string damageDisplay = "n";

	public static string witchStatus = "Ok";
	public static string suitStatus = "Ok";

	public static int witchTotalXP = 0;
	public static int suitTotalXP = 0;

	public static string selectedEnemy = "";


	public static string attButPressed = "n";

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		if ((whichTurn == 1 ) && (witchStatus == "dead"))
		{
			whichTurn = 2;
		}
		if ((whichTurn == 2 ) && (suitStatus == "dead"))
		{
			whichTurn = 1;
		}
		
	}
		
}
