﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class enemycon : MonoBehaviour {

	public float enemyHP = 100;


	public float enemyAttPow= 20;
	public int heroAttacked = 0;
	public Transform hitEffectObj;

	public int enemyXP = 100;

	SpriteRenderer m_SpriteRenderer;

	// Use this for initialization
	void Start () {

		
	}
	
	// Update is called once per frame
	void Update () {

		if ((battleflow.whichTurn == 3) && (gameObject.name == "pumpkin")) {
			GetComponent<Rigidbody> ().velocity = new Vector2 (-8, 0);
			StartCoroutine (enemyReturn ());
			battleflow.whichTurn = 4;
		}

		if ((battleflow.whichTurn == 5) && (gameObject.name == "pumpkin2")) {
			GetComponent<Rigidbody> ().velocity = new Vector2 (-8, 0);
			StartCoroutine (enemyReturn ());
			battleflow.whichTurn = 1;
		}


		if ((battleflow.damageDisplay == "y") && (gameObject.name == battleflow.selectedEnemy))
		{

			enemyHP -= battleflow.currentDamage;
			Instantiate (hitEffectObj, transform.position, hitEffectObj.rotation);
			Debug.Log (gameObject.name + "   " + enemyHP);
			battleflow.damageDisplay = "n";
		}

		if (enemyHP <= 0) {
			
			battleflow.witchTotalXP = +enemyXP;
			battleflow.suitTotalXP = +enemyXP;
			Debug.Log ("Witch XP " + battleflow.witchTotalXP + "Suit XP " + battleflow.suitTotalXP);
			Destroy (gameObject); 
		}
			
		
	}

	IEnumerator enemyReturn()
	{
		yield return new WaitForSeconds (2.3f);


		if (battleflow.whichTurn == 4) {

			battleflow.whichTurn = 5;
		}

		heroAttacked = Random.Range (1, 3);

		if (battleflow.witchStatus == "dead") {
		
			heroAttacked = 2;
		
		}

		if (battleflow.suitStatus == "dead") {

			heroAttacked = 1;

		}


		if (heroAttacked == 1) {

			witchcon.witchHP -= enemyAttPow;
			Instantiate (hitEffectObj, new Vector2(-11.01f, 2.16f), hitEffectObj.rotation);

		} else {
			
			suitcon.suitHP -= enemyAttPow;
			Instantiate (hitEffectObj, new Vector2(-10.88f, -1.97f), hitEffectObj.rotation);

		}

		Debug.Log ("Witch HP " + witchcon.witchHP);
		GetComponent<Rigidbody> ().velocity = new Vector2 (0, 0);

		if (gameObject.name == "pumpkin") {

			GetComponent<Transform> ().position = new Vector2 (9.74f, 2.25f);

		}

		if (gameObject.name == "pumpkin2") {

			GetComponent<Transform> ().position = new Vector2 (9.74f, -2.14f);

		}


	}

	void OnMouseDown(){

		battleflow.selectedEnemy = gameObject.name;
	}

	private Color startColor;

	void OnMouseEnter()
	{
		m_SpriteRenderer = GetComponent<SpriteRenderer>();
		m_SpriteRenderer.color = Color.red;

	}
	void OnMouseExit()
	{
		m_SpriteRenderer = GetComponent<SpriteRenderer>();
		m_SpriteRenderer.color = Color.white;
		
	}
}
